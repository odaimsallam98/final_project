import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:odai_final_project/Restaurant.dart';
import 'package:flutter/material.dart';
import 'package:odai_final_project/Loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart';
import 'MenuItem.dart';
import 'OrdersList.dart';
import 'OrdersListModel.dart';
int cardHeight;
class MenuItemsList extends StatefulWidget {
  final int id;
  final String name;
  MenuItemsList(this.id,this.name);
  @override
  _MenuItemsListState createState() => _MenuItemsListState(this.id,this.name);
}

class _MenuItemsListState extends State<MenuItemsList> {
  int id;
  String name;
  _MenuItemsListState(this.id,this.name);


  Future<List<MenuItem>> futureMenuItems;

  Future<List<MenuItem>> fetchMenuItems() async {

    final http.Response response = await http.get('http://appback.ppu.edu//menus/'+id.toString());

    if(response.statusCode == 200) {
      List jsonArray = jsonDecode(response.body);
      List<MenuItem> menu_items = jsonArray.map((e) => MenuItem.fromJson(e)).toList();
      return menu_items;
    }
    else{
      throw Exception("failed to load data");
    }
  }

//  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => RestaurantsList(Restaurants:Restaurants)));

  @override
  void initState() {
    super.initState();
    this.futureMenuItems = fetchMenuItems();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(name + " Menu ",
            style: TextStyle(
                color: Colors.black
            )),
            actions: [
              IconButton(
                icon: Icon(Icons.playlist_add_check),
                onPressed: () {
                 Navigator.push(
                   context,
                   MaterialPageRoute(builder: (context) => OrdersList()),
                 );
                },
              ),
            ],
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
        ),
        body:
        FutureBuilder<List<MenuItem>>(
            future:this.futureMenuItems,
            builder: (context,snapshot){

              if(snapshot.hasData){
                List<MenuItem> menuItems = snapshot.data;
                return

                  CarouselSlider(
                    options:
                    CarouselOptions(
                      height: (0.8*(MediaQuery. of(context). size. height)),
                      aspectRatio: 16/9,
                      viewportFraction: 0.8,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      reverse: false,
                      autoPlay: true,
                      autoPlayInterval: Duration(seconds: 3),
                      autoPlayAnimationDuration: Duration(milliseconds: 800),
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enlargeCenterPage: true,
                     // onPageChanged: callbackFunction,
                      scrollDirection: Axis.horizontal,

                    ),
                    items: menuItems.map((i) {
                      return Builder(
                        builder: (BuildContext context) {
                          return MenuItemWidget(i,1);
                        },
                      );
                    }).toList(),
                  )

/*
                  CarouselSlider.builder(
                      itemCount: menuItems.length,
                      itemBuilder: (BuildContext context, int index){
                        return MenuItemWidget(menuItems[index],index);
                      },
*/
                /*
                  ListView.builder(
                  itemCount: menuItems.length,
                  itemBuilder: (BuildContext context, int index){
                    return MenuItemWidget(menuItems[index],index);
                  },
                )

                */

                  ;
              }
              else if(snapshot.hasError){
                return Text("error ${snapshot.error}");
              }
              return Center(
                child: SpinKitPouringHourglass(
                  color: Colors.black,
                  size: 50.0,
                ),
              );
            }
        )
    );
  }
}

/*

class MenuItemWidget extends StatelessWidget {

  final MenuItem _menu_item;
  final int index;
  MenuItemWidget(this._menu_item,this.index);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(4),
        height: 120,
        child: Card(
          child: Row(
            children: [
              Image.network("http://appback.ppu.edu/static/"+this._menu_item.image),
              Expanded(
                  child: Container(
                    padding: EdgeInsets.fromLTRB(20, 5, 5, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          this._menu_item.name, style: TextStyle(fontWeight: FontWeight.bold),),
                        Text(this._menu_item.description),
                        Row(
                          children: [
                             Consumer<OrdersListModel>
                              (builder: (context, ordersList, child){
                              bool addedToOrdersList = ordersList.itemExists(_menu_item);
                              return  IconButton(
                                icon: Icon(
                                  addedToOrdersList? Icons.delete :Icons.add,
                               ),
                              onPressed: () {
                                ordersList.itemExists(_menu_item)?
                              Provider.of<OrdersListModel>(context, listen: false).remove(_menu_item):
                                Provider.of<OrdersListModel>(context, listen: false).add(this._menu_item);
                                }
                              );
                              }),
                            Expanded(
                              child:  Container(
                                alignment: Alignment.centerRight,
                                child: Text(this._menu_item.price.toString()),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )
              )
            ],
          ),
        ),
      );
  }
}

*/

class MenuItemWidget extends StatelessWidget {
  final MenuItem _menu_item;
  final int index;
  MenuItemWidget(this._menu_item,this.index);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.white, Colors.blue])),

      margin: new EdgeInsets.symmetric(vertical: 40),
      child: Column(

        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Card(
            child:Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children:[
                Container(
                    height: 200,
                      child: Image.network("http://appback.ppu.edu/static/"+this._menu_item.image)
                    ),
                Text(
                        this._menu_item.name, style: TextStyle(fontWeight: FontWeight.bold),),
                Text(this._menu_item.description),
                Consumer<OrdersListModel>
                      (builder: (context, ordersList, child){
                              bool addedToOrdersList = ordersList.itemExists(_menu_item);
                  return  IconButton(
                    icon: Icon(
                  addedToOrdersList? Icons.delete :Icons.add,
                ),
                onPressed: () {
                  ordersList.itemExists(_menu_item)?
                  Provider.of<OrdersListModel>(context, listen: false).remove(_menu_item):
                  Provider.of<OrdersListModel>(context, listen: false).add(this._menu_item);
                }
                    );
                        } ),
                Text('Price:'+this._menu_item.price.toString())
                ])
    )
        ],

      ),
    );
  }
}


