import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import 'Restaurant.dart';
import 'MenuItem.dart';
import 'RestaurantsList.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {

  Future<List<Restaurant>> restaurants;

  Future<List<Restaurant>> fetchRestaurants() async {

    final http.Response response = await http.get('http://appback.ppu.edu/restaurants');

    if(response.statusCode == 200) {
      List jsonArray = jsonDecode(response.body);
      List<Restaurant> restaurants = jsonArray.map((e) => Restaurant.fromJson(e)).toList();
      return restaurants;
    }
    else{
      throw Exception("failed to load data");
    }
  }

//  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => RestaurantsList(Restaurants:Restaurants)));

  @override
  void initState() {
    super.initState();
    this.restaurants = fetchRestaurants();
  }

  @override
  Widget build(BuildContext context){
    return FutureBuilder<List<Restaurant>>(
        future:this.restaurants,
        builder: (context,snapshot){

          if(snapshot.hasData){
           List<Restaurant> res = snapshot.data;
           return RestaurantsList(restaurants:res);
          }
          else if(snapshot.hasError){
            return Text("error ${snapshot.error}");
          }
          return Center(
            child: SpinKitPouringHourglass(
              color: Colors.black,
              size: 50.0,
            ),
          );
        }
    );
  }

}
