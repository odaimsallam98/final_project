
class MenuItem{
  final String name;
  final String description;
  final int price;
  final String image;
 // final double rating;
  MenuItem({this.name,this.description,this.image,this.price,
   // this.rating
  });

  factory MenuItem.fromJson(dynamic jsonObject){
    return MenuItem(
        name: jsonObject['name'],
        description: jsonObject['descr'],
        image: jsonObject['image'],
        price: int.parse(jsonObject['price'].toString()),
       // rating: double.parse(jsonObject['rating'].toString()),
    );
  }
}