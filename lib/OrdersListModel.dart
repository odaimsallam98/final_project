import 'package:flutter/cupertino.dart';
import 'package:odai_final_project/MenuItem.dart';

import 'Restaurant.dart';

class OrdersListModel extends ChangeNotifier{
  final List<MenuItem> _ordersList = [];

  int get numItems => _ordersList.length;
  MenuItem getItem(int index) => _ordersList[index];

  bool itemExists(MenuItem MI) => _ordersList.indexOf(MI)>=0 ? true: false;

  void add(MenuItem menuItem){
    _ordersList.add(menuItem);
    notifyListeners();
  }

  void remove(MenuItem MI){
    _ordersList.remove(MI);
    notifyListeners();
  }

  void removeAll(){
    _ordersList.clear();
    notifyListeners();
  }
}