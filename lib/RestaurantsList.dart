import 'package:flutter/material.dart';
import 'package:odai_final_project/Restaurant.dart';
import 'package:flutter/material.dart';
import 'package:odai_final_project/Loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart';

import 'MenuItemsList.dart';

class RestaurantsList extends StatefulWidget {
  final List<Restaurant> restaurants;
  RestaurantsList({this.restaurants});

  @override
  _RestaurantsListState createState() => _RestaurantsListState(restaurants);
}

class _RestaurantsListState extends State<RestaurantsList> {

  List<Restaurant> restaurants;

  _RestaurantsListState(this.restaurants);

  @override
  Widget build(BuildContext context) {
    return  CarouselSlider(
      options:
      CarouselOptions(
        height: ((MediaQuery. of(context). size. height)),

        viewportFraction: 0.8,
        initialPage: 0,
        enableInfiniteScroll: false,
        reverse: false,

        autoPlayInterval: Duration(seconds: 3),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,


        aspectRatio: 2.0,
        enlargeCenterPage: true,
        scrollDirection: Axis.vertical,
        autoPlay: true,
        // onPageChanged: callbackFunction,


      ),
      items: restaurants.map((i) {
        return Builder(
          builder: (BuildContext context) {
            return RestaurantItem(i,1);
          },
        );
      }).toList(),
    )

      /*
      ListView.builder(
        itemCount: restaurants.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: RestaurantItem(restaurants[index],index),
          );
        }
    )*/

    ;
  }
}

/*
class RestaurantItem extends StatelessWidget {

  final Restaurant _restaurant;
  final int index;
  RestaurantItem(this._restaurant,this.index);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MenuItemsList(_restaurant.id,_restaurant.name)),
          );
        },
      child: Container(
        padding: EdgeInsets.all(4),
        height: 120,
        child: Card(
          child: Row(
            children: [
              Image.network("http://appback.ppu.edu/static/"+this._restaurant.image_name),
              Expanded(
                  child: Container(
                    padding: EdgeInsets.fromLTRB(20, 5, 5, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          this._restaurant.name, style: TextStyle(fontWeight: FontWeight.bold),),
                        Text(this._restaurant.city),
                        Row(
                          children: [
                           // Consumer<WatchListModel>
                            //  (builder: (context, watchList, child){
                            //  bool addedToWatchList = watchList.itemExists(movie);
                             // return  IconButton(
                                //  icon: Icon(
                                //    addedToWatchList? Icons.delete :Icons.watch_later,
                               //   ),
                                //  onPressed: () {
                                //    watchList.itemExists(movie)?
                                  //  Provider.of<WatchListModel>(context, listen: false).remove(movie):
                                //    Provider.of<WatchListModel>(context, listen: false).add(this.movie);
                              //    }
                            //  );
                          //  }),
                            Expanded(
                              child:  Container(
                                alignment: Alignment.centerRight,
                                child: Text(this._restaurant.rating.toString()),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )
              )
            ],
          ),
        ),
      ),
    );
  }
}

*/

class RestaurantItem extends StatelessWidget {
  final Restaurant _restaurant;
  final int index;
  RestaurantItem(this._restaurant,this.index);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MenuItemsList(_restaurant.id,_restaurant.name)),
        );
      },
      child:Card(
          child:Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.white, Colors.black])),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:[
                  Container(
                      height: 200,
                      width:  0.7*((MediaQuery. of(context). size.width)),
                      child: Image.network("http://appback.ppu.edu/static/"+this._restaurant.image_name)
                  ),
                  Text(
                    this._restaurant.name, style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),),
                  Text(this._restaurant.city,style: TextStyle(color: Colors.white),),
                  Text('Price:'+this._restaurant.rating.toString(),style: TextStyle(color: Colors.white),)
                ]),
          )

      )
    );
  }
}


/*

Card(
                  child:Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children:[
                        Container(
                            height: 200,
                            child: Image.network("http://appback.ppu.edu/static/"+this._restaurant.image_name)
                        ),
                        Text(
                          this._restaurant.name, style: TextStyle(fontWeight: FontWeight.bold),),
                        Text(this._restaurant.city),
                        Text('Price:'+this._restaurant.rating.toString())
                      ])
              )

*/