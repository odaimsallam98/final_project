import 'package:flutter/material.dart';
import 'package:odai_final_project/Loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'Home.dart';
import 'OrdersList.dart';
import 'OrdersListModel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => OrdersListModel(),
        child: MaterialApp(
          title: 'Flutter Demo',
          initialRoute: '/',
          routes: {
            '/': (context) => Home(),
            '/Orders': (context) => OrdersList(),

          },
        )
    );
  }
}
















/*MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home:Loading(),
    )*/
/*
class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Restaurants List'),
        /*
          actions: [
          IconButton(
            icon: Icon(Icons.playlist_add_check),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => OrdersList()),
              );
            },
          ),
        ],
        */
      ),
      body: Loading(),
    );
  }
}
*/