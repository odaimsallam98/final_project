import 'dart:convert';
import 'package:http/http.dart' as http;
import 'MenuItem.dart';

class Restaurant{
  final int id;
  final String name;
  final String city;
  final double latitude ;
  final double longitude;
  final double rating;
  final String image_name;
  final String phone;
  //final Future<List<MenuItem>> menu_items ;

  Restaurant({this.name,this.id,this.image_name,this.city,this.latitude,
    this.longitude,this.phone,this.rating,
   // this.menu_items
  });

/*
  static Future<List<MenuItem>> fetchMenuItem(int id) async{
   final http.Response response = await http.get('http://appback.ppu.edu/menus/'+(id).toString());
   if(response.statusCode == 200)
     {
       List jsonArray = jsonDecode(response.body);
       List<MenuItem> menuitems = jsonArray.map((e) => MenuItem.fromJson(e)).toList();
       return menuitems;
     }
   else{
     throw Exception("failed to load data");
   }
  }
*/

  factory Restaurant.fromJson(dynamic jsonObject){

    int _id = int.parse(jsonObject['id'].toString());

    return Restaurant(
        id: _id,
        name: jsonObject['name'],
        city: jsonObject['city'],
        latitude: double.parse(jsonObject['lat'].toString()),
        longitude: double.parse(jsonObject['lng'].toString()),
        image_name: jsonObject['image'],
        rating: double.parse(jsonObject['rating'].toString()),
        phone: jsonObject['phone'],
        //menu_items: fetchMenuItem(_id)
       );
  }
}
//jsonObject['lat'].toString()