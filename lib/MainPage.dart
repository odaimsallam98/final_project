import 'package:flutter/material.dart';
import 'package:odai_final_project/Loading.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart';
import 'Home.dart';
import 'OrdersList.dart';
import 'OrdersListModel.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Restaurants List'
        ,
        style: TextStyle(
          color: Colors.black
        ),
        ),
        backgroundColor: Colors.white,
        /*
        actions: [
          IconButton(
            icon: Icon(Icons.playlist_add_check),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => OrdersList()),
              );
            },
          ),
        ],
        */
      ),
      body: Loading(),
    );
  }
}
